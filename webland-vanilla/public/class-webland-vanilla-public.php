<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.weblandhosting.com/
 * @since      1.0.0
 *
 * @package    WebLand_Vanilla
 * @subpackage WebLand_Vanilla/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    WebLand_Vanilla
 * @subpackage WebLand_Vanilla/public
 * @author     Gerald Bullard Jr <gbullard@weblandhosting.com>
 */
class WebLand_Vanilla_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {
		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in WebLand_Vanilla_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The WebLand_Vanilla_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/webland-vanilla-public.css', array(), $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in WebLand_Vanilla_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The WebLand_Vanilla_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/webland-vanilla-public.js', array( 'jquery' ), $this->version, false );
	}

	function load_custom_webland_vanilla_templates( $template ) {
		if ( is_single() && get_post_type() === 'webland_vanilla' ) {
			return plugin_dir_path( __FILE__ ) . 'templates/single-webland_vanilla.php';
		} elseif ( is_tax( 'webland_vanilla_taxonomy' ) ) {
			return plugin_dir_path( __FILE__ ) . 'templates/taxonomy-webland_vanilla_taxonomy.php';
		}
		return $template;
	}

}
