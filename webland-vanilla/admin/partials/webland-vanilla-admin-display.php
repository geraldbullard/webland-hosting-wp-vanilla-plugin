<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://www.weblandhosting.com/
 * @since      1.0.0
 *
 * @package    WebLand_Vanilla
 * @subpackage WebLand_Vanilla/admin/partials
 */

// Admin page html callback
function webland_vanilla_html()
{
    // Check user capabilities
    if (!current_user_can('manage_options')) {
        return;
    }
    $webland_vanilla = new WebLand_Vanilla();
    ?>
    <div id="wlh-dashboard" class="wrap">
        <?php include_once(__DIR__ . '/webland-admin-header.php'); ?>
        <main class="content">
            <h1><?php echo esc_html('WebLand Vanilla Plugin'); ?></h1>
            <?php include_once(__DIR__ . '/webland-admin-main.php'); ?>
        </main>
    </div>
    <?php
}

// Admin Settings page html callback
function webland_vanilla_settings_html()
{
    // Check user capabilities
    if (!current_user_can('manage_options')) {
        return;
    }
    $webland_vanilla = new WebLand_Vanilla();
    // Get the active tab from the $_GET param
    $default_tab = null;
    $tab = isset($_GET['tab']) ? $_GET['tab'] : $default_tab;
    ?>
    <div id="wlh-dashboard" class="wrap">
        <?php include_once(__DIR__ . '/webland-admin-header.php'); ?>
        <main class="content">
            <h1><?php echo esc_html('WebLand Vanilla Settings'); ?></h1>
            <nav class="nav-tab-wrapper">
                <a href="?page=webland-vanilla-settings" class="nav-tab<?php if ($tab === null): ?> nav-tab-active<?php endif; ?>">Settings</a>
                <!--<a href="?page=webland-vanilla-settings&tab=premium" class="nav-tab<?php if ($tab === 'premium'): ?> nav-tab-active<?php endif; ?>">Premium</a>-->
            </nav>
            <div class="tab-content">
                <?php
                    switch ($tab) :
                        // case 'premium':
                        //     include_once(__DIR__ . '/tabs/webland-vanilla-admin-tabs-premium.php');
                        //     break;
                        default:
                            include_once(__DIR__ . '/tabs/webland-vanilla-admin-tabs-settings.php');
                            break;
                    endswitch;
                ?>
            </div>
        </main>
    </div>
    <?php
}

// Admin page html callback
function webland_vanilla_help_html()
{
    // Check user capabilities
    if (!current_user_can('manage_options')) {
        return;
    }
    $webland_vanilla = new WebLand_Vanilla();
    ?>
    <div id="wlh-dashboard" class="wrap">
        <?php include_once(__DIR__ . '/webland-admin-header.php'); ?>
        <main class="content">
            <h1><?php echo esc_html('WebLand Vanilla Help'); ?></h1>
            <?php include_once(__DIR__ . '/webland-admin-help.php'); ?>
        </main>
    </div>
    <?php
}

/*function webland_vanilla_taxonomies_html() {
    ?>
    <div class="wrap">
        <h1>WebLand Vanilla Categories</h1>

        <?php
        if (isset($_POST['submit'])) {
            // Process form data here, such as adding or updating taxonomy categories
            // You can use $_POST['category_name'] and other fields from your form
        }
        ?>

        <form method="post" action="">
            <label for="category_name">Category Name:</label>
            <input type="text" name="category_name" id="category_name">
            <input type="submit" name="submit" value="Add Category">
        </form>

        <h2>Existing Categories</h2>
        <ul>
            <?php
            // Display existing taxonomy categories
            $categories = get_terms('your_taxonomy_name', array('hide_empty' => false));
            foreach ($categories as $category) {
                echo '<li>' . esc_html($category->name) . '</li>';
            }
            ?>
        </ul>
    </div>
    <?php
}*/