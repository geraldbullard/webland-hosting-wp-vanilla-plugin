
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <a class="navbar-brand" href="/wp-admin/admin.php?page=webland-vanilla">
                <img src="/wp-content/plugins/webland-vanilla/public/img/fs-logo-85.png" height="32" class="d-inline-block align-top" alt="WebLand Vanilla">
            </a>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="admin.php?page=webland-vanilla">Features</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin.php?page=webland-vanilla-settings">Settings</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="admin.php?page=webland-vanilla-help">Help</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://www.geraldbullardjr.com/" target="_blank">Go Pro</a>
                </li>
            </ul>
            <aside>
                WebLand Vanilla Version: <?php echo WEBLAND_VANILLA_VERSION; ?>
                <div class="wp-menu-image dashicons-before dashicons-superhero"></div>
            </aside>
        </nav>