<?php

/**
 * Define the plugin post types
 *
 * Loads and defines the post types for this plugin.
 *
 * @link       https://www.weblandhosting.com/
 * @since      1.0.0
 * @package    WebLand_Vanilla
 * @subpackage WebLand_Vanilla/includes
 * @author     Gerald Bullard Jr <gbullard@weblandhosting.com>
 */
class WebLand_Vanilla_PostTypes {
    
    // Define Post Types
	public function define_plugin_post_types() {
        // Register custom post type
        $cptlabels = array(
            'name'               => _x( 'WLH CPT', 'Post Type General Name', 'webland-vanilla' ),
            'singular_name'      => _x( 'WLH CPT', 'Post Type Singular Name', 'webland-vanilla' ),
            'menu_name'          => __( 'WLH CPT', 'webland-vanilla' ),
            'name_admin_bar'     => __( 'WLH CPT', 'webland-vanilla' ),
            'parent_item_colon'  => __( 'Parent WLH CPT', 'webland-vanilla' ),
            'all_items'          => __( 'All WLH CPT', 'webland-vanilla' ),
            'add_new_item'       => __( 'Add New WLH CPT', 'webland-vanilla' ),
            'add_new'            => __( 'Add New WLH CPT', 'webland-vanilla' ),
            'new_item'           => __( 'New WLH CPT', 'webland-vanilla' ),
            'edit_item'          => __( 'Edit WLH CPT', 'webland-vanilla' ),
            'update_item'        => __( 'Update WLH CPT', 'webland-vanilla' ),
            'view_item'          => __( 'View WLH CPT', 'webland-vanilla' ),
            'search_items'       => __( 'Search WLH CPT', 'webland-vanilla' ),
            'not_found'          => __( 'Not found', 'webland-vanilla' ),
            'not_found_in_trash' => __( 'Not found in Trash', 'webland-vanilla' ),
        );
        $cptargs = array(
            'label'              => __( 'webland_vanilla_label', 'webland-vanilla' ),
            'description'        => __( 'WLH CPT', 'webland-vanilla' ),
            'labels'             => $cptlabels,
            'supports'           => array( 'title', 'editor', 'author','thumbnail', 'excerpt', 'revisions' ),
            'taxonomies'         => array( 'webland_vanilla_taxonomy' ),
            'public'             => true,
            'hierarchical'       => true,
            'menu_position'      => 36,
            'menu_icon'          => 'dashicons-superhero',
            'map_meta_cap'       => true,
            'show_in_rest' 		 => true,
            'has_archive'        => true,
            'rewrite'            => true,
        );
        register_post_type( 'webland_vanilla', $cptargs );

        // Add custom taxonomy
        register_taxonomy(
            'webland_vanilla_taxonomy', 
            array( 'webland_vanilla' ), 
            array(
                // Hierarchical taxonomy (like categories)
                'hierarchical' => true,
                // This array of options controls the labels displayed in the WordPress Admin UI
                'labels' => array(
                    'name'                       => _x( 'WLH CPT Taxonomies', 'Taxonomy General Name', 'webland-vanilla' ),
                    'singular_name'              => _x( 'WLH CPT Taxonomy', 'Taxonomy Singular Name', 'webland-vanilla' ),
                    'search_items'               => __( 'Search WLH CPT Taxonomies' ),
                    'all_items'                  => __( 'All WLH CPT Taxonomies' ),
                    'parent_item'                => __( 'Parent WLH CPT Taxonomy' ),
                    'parent_item_colon'          => __( 'Parent WLH CPT Taxonomy:' ),
                    'edit_item'                  => __( 'Edit WLH CPT Taxonomy' ),
                    'update_item'                => __( 'Update WLH CPT Taxonomy' ),
                    'add_new_item'               => __( 'Add New WLH CPT Taxonomy' ),
                    'new_item_name'              => __( 'New WLH CPT Taxonomy' ),
                    'menu_name'                  => __( 'WLH CPT Taxonomies' ),
                    'view_item'                  => __( 'View WLH CPT', 'webland-vanilla' ),
                    'separate_items_with_commas' => __( 'Separate WLH CPT Taxonomies With Commas', 'webland-vanilla' ),
                    'add_or_remove_items'        => __( 'Add or Remove WLH CPT Taxonomies', 'webland-vanilla' ),
                    'choose_from_most_used'      => __( 'Choose From Most Used', 'webland-vanilla' ),
                    'popular_items'              => __( 'Popular WLH CPT Taxonomies', 'webland-vanilla' ),
                    'not_found'                  => __( 'Not Found', 'webland-vanilla' ),
                    'no_terms'                   => __( 'No WLH CPT Taxonomies', 'webland-vanilla' ),
                    'items_list'                 => __( 'WLH CPT Taxonomies List', 'webland-vanilla' ),
                    'items_list_navigation'      => __( 'WLH CPT Taxonomies List Navigation', 'webland-vanilla' ),
                ),
                // Control the slugs used for this taxonomy
                'rewrite' => array(
                    'slug' => 'webland-vanilla-taxonomy',
                    'with_front' => false,
                    'hierarchical' => true
                ),
                'public' => true,
                'show_ui' => true,
                'show_admin_column' => true,
                'query_var' => true,
            )
        );

        function webland_vanilla_taxonomy_meta_box() {
            // Define the ID, title, callback function, and screen context
            add_meta_box(
                'webland_vanilla_taxonomy_meta_box',
                'WLH&nbsp;CPT&nbsp;Taxonomy',
                'webland_vanilla_taxonomy_meta_box_callback',
                'webland_vanilla',
                'side',
                'default'
            );
        }
        add_action( 'admin_init', 'webland_vanilla_taxonomy_meta_box' );

        function webland_vanilla_taxonomy_meta_box_callback( $post ) {
            // Display the terms and input field to assign terms
            $taxonomy = 'webland_vanilla_taxonomy';
            $terms = get_terms( $taxonomy, array( 'hide_empty' => false ) );
            $selected_terms = wp_get_post_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
            // Add an nonce field so we can check for it later.
            wp_nonce_field( __FILE__, 'webland_vanilla_taxonomy_nonce' );

            if ( $terms ) {
                echo '<ul>';
                foreach ( $terms as $term ) {
                    echo '<li><label>';
                    echo '<input type="checkbox" name="webland_vanilla_taxonomy[]" value="' . esc_attr( $term->term_id ) . '"';
                    if ( in_array( $term->term_id, $selected_terms ) ) {
                        echo ' checked="checked"';
                    }
                    echo '>';
                    echo esc_html( $term->name );
                    echo '</label></li>';
                }
                echo '</ul>';
            }
        }

        function save_webland_vanilla_taxonomy( $post_id ) {
            global $pagenow;
            if ( $pagenow === 'post.php' ) {
                $selected_terms = array_map( 'intval', $_POST['webland_vanilla_taxonomy'] );
                if ( isset( $_POST['webland_vanilla_taxonomy'] ) ) {
                    wp_set_post_terms( $post_id, $selected_terms, 'webland_vanilla_taxonomy', false );
                } else {
                    wp_set_post_terms( $post_id, array(), 'webland_vanilla_taxonomy', false );
                }
            }
        }
        add_action( 'save_post', 'save_webland_vanilla_taxonomy', 1, 2 );
	}

}
