<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://www.weblandhosting.com/
 * @since      1.0.0
 * @package    WebLand_Vanilla
 * @subpackage WebLand_Vanilla/includes
 * @author     Gerald Bullard Jr <gbullard@weblandhosting.com>
 */
class WebLand_Vanilla_i18n {

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain(
			'webland-vanilla',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);
	}

}
