<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.geraldbullardjr.com/
 * @since      1.0.0
 * @package    WebLand_Vanilla
 * @subpackage WebLand_Vanilla/includes
 * @author     Gerald Bullard Jr <gbj@geraldbullardjr.com>
 */
class WebLand_Vanilla_Activator {
	/**
	 * @var webland_vanilla Vanilla table name
	 */
	private $table_name;

	/**
	 * Short Description.
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		// Let's create a simple table if it doesn't exist
		global $wpdb;
		$table_name = $wpdb->prefix . 'webland_vanilla';
		if (self::is_table_not_exists($table_name)) {
			self::create_table($table_name);
		}
	}
	
	/**
	 * Check if Vanilla table exists
	 */
	private function is_table_not_exists($table_name) {
		global $wpdb;
		return ( $wpdb->get_var( "SHOW TABLES LIKE '{$table_name}'" ) != $table_name );
	}

	/**
	 * Create Vanilla table
	 */
	private function create_table($table_name) {
		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();
		$sql = "CREATE TABLE {$table_name} (
			id BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
			url VARCHAR(50) NOT NULL DEFAULT 0,
    		PRIMARY KEY (`id`),
    		KEY `idx_url` (`url`)
		) $charset_collate;";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );
	}
}
