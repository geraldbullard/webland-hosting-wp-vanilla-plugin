<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @link       https://www.geraldbullardjr.com/
 * @since      1.0.0
 * @package    WebLand_Vanilla
 * @subpackage WebLand_Vanilla/includes
 * @author     Gerald Bullard Jr <gbj@geraldbullardjr.com>
 */
class WebLand_Vanilla {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      WebLand_Vanilla_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	//protected $webland_vanailla_verified;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'WEBLAND_VANILLA_VERSION' ) ) {
			$this->version = WEBLAND_VANILLA_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'webLand_vanilla';
		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->define_shortcodes();
		$this->define_post_types();

        // Check the API for paid version //////////////////////////////////////////////////////////////////////////////
        // $url = "https://api.weblandhosting.com/auth/login";
        // $params = array(
        //     'username' => esc_attr(get_option('webland_vanilla_username', '')),
        //     'password' => esc_attr(get_option('webland_vanilla_password', '')),
        // );
        // echo '<pre>';
        // print_r($params);
        // echo '</pre>';
        // echo '[ ' . $url . '?username=' . $params['username'] . '&password=' . $params['password'] . ' ]';
        // $ch = curl_init();
        // curl_setopt($ch, CURLOPT_URL, $url . '?username=' . $params['username'] . '&password=' . $params['password']);
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        //     'Client-Service: wlh-api-client',
        //     'Auth-Key: wlhapiauthkey',
        //     'Content-Type: application/x-www-form-urlencoded'
        // ));
        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        // curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        // $response = curl_exec($ch);
        // curl_close($ch);
        // $result = json_decode($response);
        // echo '<pre>';
        // print_r($result);
        // echo '</pre>';
        // $this->webland_vanilla_verified = false;
        // if ($result->status == '200') {
        //     // Later we will check for the correct product that matches the plugin name like 'vanilla'
        //     $this->webland_vanilla_verified = true;
        //     $this->webland_vanilla_premium_html = $result->premium_html;
        // }
        // echo '<pre>';
        // echo '[' . $this->webland_vanilla_verified . ']';
        // echo '</pre>';
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - WebLand_Vanilla_Loader. Orchestrates the hooks of the plugin.
	 * - WebLand_Vanilla_i18n. Defines internationalization functionality.
	 * - WebLand_Vanilla_Admin. Defines all hooks for the admin area.
	 * - WebLand_Vanilla_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {
		/**
		 * The class responsible for orchestrating the actions and filters of the core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-webLand-vanilla-loader.php';
		/**
		 * The class responsible for defining internationalization functionality of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-webLand-vanilla-i18n.php';
		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-webLand-vanilla-admin.php';
		/**
		 * The class responsible for defining all html that displays in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/webLand-vanilla-admin-display.php';
		/**
		 * The class responsible for defining all actions that occur in the public-facing side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-webLand-vanilla-public.php';
		/**
		 * The class responsible for defining all shortcodes needed to support the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-webland-vanilla-shortcodes.php';
		/**
		 * The class responsible for defining post types needed to support the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-webland-vanilla-post-types.php';
		$this->loader = new WebLand_Vanilla_Loader();
	}

	private function define_shortcodes() {
		$plugin_shortcodes = new WebLand_Vanilla_Shortcodes();
		$this->loader->add_action( 'plugins_loaded', $plugin_shortcodes, 'define_plugin_shortcodes' );		
		// Flush rewrite rules to ensure shortcodes work
		flush_rewrite_rules();
	}

	private function define_post_types() {
		$plugin_post_types = new WebLand_Vanilla_PostTypes();
		$this->loader->add_action( 'init', $plugin_post_types, 'define_plugin_post_types' );
	}	

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the WebLand_Vanilla_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {
		$plugin_i18n = new WebLand_Vanilla_i18n();
		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {
		// Get the version
	    $plugin_admin = new WebLand_Vanilla_Admin( $this->get_plugin_name(), $this->get_version() );
        // Load the styles
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		// Load the scripts
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		// Add the Admin Menu/Submenus
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'add_admin_menu' );
		// Hide the admin Settings entry menu item
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'remove_settings_submenu', 99 );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {
		$plugin_public = new WebLand_Vanilla_Public( $this->get_plugin_name(), $this->get_version() );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_filter( 'template_include', $plugin_public, 'load_custom_webland_vanilla_templates' );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    WebLand_Vanilla_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	// public function get_vanilla_verified() {
	// 	return $this->webland_vanilla_verified;
	// }

	// public function get_vanilla_premium_html() {
	// 	return $this->webland_vanilla_premium_html;
	// }

}
