<?php

/**
 * Define the plugin shortcodes
 *
 * Loads and defines the shortcodes for this plugin to support the front end display.
 *
 * @link       https://www.weblandhosting.com/
 * @since      1.0.0
 * @package    WebLand_Vanilla
 * @subpackage WebLand_Vanilla/includes
 * @author     Gerald Bullard Jr <gbullard@weblandhosting.com>
 */
class WebLand_Vanilla_Shortcodes {
    
    // Define Shortcodes
	public function define_plugin_shortcodes() {
        
		// Callback function for your main shortcode
		function webland_vanilla_shortcode_main_callback($atts, $content = null) {
			$html = '';
			/*$html .= '<div class="wv-breadcrumbs">';		
			// Home link
			$html .= '<a href="' . esc_url( home_url() ) . '">' . __( 'Home', 'webland_vanilla' ) . '</a>';
			// Custom post type taxonomy page check
			if ( is_tax( 'webland_vanilla_taxonomy' ) ) {
				$taxonomy = get_queried_object();				
				// Get the taxonomy hierarchy
				$ancestors = get_ancestors( $taxonomy->term_id, 'webland_vanilla_taxonomy' );
				if ( $ancestors ) {
					$ancestors = array_reverse( $ancestors );
					foreach ( $ancestors as $ancestor_id ) {
						$ancestor = get_term( $ancestor_id, 'webland_vanilla_taxonomy' );
						$html .= '<span class="wv-breadcrumb-separator">&raquo;</span>';
						$html .= '<a href="' . esc_url( get_term_link( $ancestor ) ) . '">' . esc_html( $ancestor->name ) . '</a>';
					}
				}
				// Current taxonomy term
				$html .= '<span class="wv-breadcrumb-separator">&raquo;</span>';
				$html .= esc_html( $taxonomy->name );
			}			
			$html .= '</div>';*/

			// Taxonomies work
			$taxonomy = 'webland_vanilla_taxonomy';
			$terms = get_terms( array(
				'taxonomy'   => $taxonomy,
				'hide_empty' => false,
				'orderby'    => 'id', // Sort by term name
				'order'      => 'ASC',  // Ascending order
			) );

			if ( ! empty( $terms ) ) {
				$html .= '<div class="wv-grid" id="wv-tax-topics">' . "\n";
				foreach ( $terms as $term ) {
					$html .= '<div class="wv-item wv-bit-33"><a href="' . esc_url( get_term_link( $term ) ) . '">' . esc_html( $term->name ) . '</a></div>';
				}
				$html .= '</div>';
			} else {
				$html .= 'No terms found.';
			}
			////////////////////////////////////////////////////////////////

			// Shortcode logic and content generation
			// You can use $atts to retrieve attributes passed to the shortcode
			// $content contains any content enclosed within the shortcode tag
			
			// Return the shortcode content
			//$html .= '<div class="webland-vanilla-shortcode">' . esc_html__('WebLand Vanilla Shortcode Content!', 'webland-vanilla') . '</div><br />';
			$html .= '<br /><h3>WebLand Vanilla Custom CSS Grid System (1-100)</h3>' . "\n";
			// 57-43
			$html .= '<div class="wv-grid">' . "\n";
			$html .= '<div class="wv-item wv-bit-57">57% width</div>' . "\n";
			$html .= '<div class="wv-item wv-bit-43">43% width</div>' . "\n";
			$html .= '</div>' . "\n";
			// 33-67
			$html .= '<div class="wv-grid">' . "\n";
			$html .= '<div class="wv-item wv-bit-33">33% width</div>' . "\n";
			$html .= '<div class="wv-item wv-bit-67">67% width</div>' . "\n";
			$html .= '</div>' . "\n";
			// 22-78
			$html .= '<div class="wv-grid">' . "\n";
			$html .= '<div class="wv-item wv-bit-78">78% width</div>' . "\n";
			$html .= '<div class="wv-item wv-bit-22">22% width</div>' . "\n";
			$html .= '</div>' . "\n";
			// 50-50
			$html .= '<div class="wv-grid">' . "\n";
			$html .= '<div class="wv-item wv-bit-50">50% width</div>' . "\n";
			$html .= '<div class="wv-item wv-bit-50">50% width</div>' . "\n";
			$html .= '</div>' . "\n";
			// 25-25-25-25
			$html .= '<div class="wv-grid">' . "\n";
			$html .= '<div class="wv-item wv-bit-25">25% width</div>' . "\n";
			$html .= '<div class="wv-item wv-bit-25">25% width</div>' . "\n";
			$html .= '<div class="wv-item wv-bit-25">25% width</div>' . "\n";
			$html .= '<div class="wv-item wv-bit-25">25% width</div>' . "\n";
			$html .= '</div>' . "\n";
			return $html;
		}
		// Define the main shortcode
		add_shortcode('webland_vanilla_shortcode_main', 'webland_vanilla_shortcode_main_callback');	

		function webland_vanilla_breadcrums() {
			$breadcrumbs = '<div class="breadcrumbs">';
			// Change /webland-vanilla/ to a setting controlled value based on custom post type main page where shortcode lives
			$breadcrumbs .= '<a href="/webland-vanilla/">' . __( 'Webland Vanilla', 'webland_vanilla' ) . '</a>';
			// Custom post type taxonomy page check
			if ( is_single() && get_post_type() === 'webland_vanilla' ) {
				global $post;
				$terms = wp_get_post_terms( $post->ID, 'webland_vanilla_taxonomy' );

				if ( $terms && ! is_wp_error( $terms ) ) {
					$term = $terms[0];
					$ancestors = get_ancestors( $term->term_id, 'webland_vanilla_taxonomy' );
					if ( $ancestors ) {
						$ancestors = array_reverse( $ancestors );
						foreach ( $ancestors as $ancestor_id ) {
							$ancestor = get_term( $ancestor_id, 'webland_vanilla_taxonomy' );
							$breadcrumbs .= '<span class="breadcrumb-separator"> &raquo; </span>';
							$breadcrumbs .= '<a href="' . esc_url( get_term_link( $ancestor ) ) . '">' . esc_html( $ancestor->name ) . '</a>';
						}
					}
					$breadcrumbs .= '<span class="breadcrumb-separator">&raquo;</span>';
					$breadcrumbs .= '<a href="' . esc_url( get_term_link( $term ) ) . '">' . esc_html( $term->name ) . '</a>';
				}
			} elseif ( is_tax( 'webland_vanilla_taxonomy' ) ) {
				$taxonomy = get_queried_object();
				
				// Get the taxonomy hierarchy
				$ancestors = get_ancestors( $taxonomy->term_id, 'webland_vanilla_taxonomy' );
				if ( $ancestors ) {
					$ancestors = array_reverse( $ancestors );
					foreach ( $ancestors as $ancestor_id ) {
						$ancestor = get_term( $ancestor_id, 'webland_vanilla_taxonomy' );
						$breadcrumbs .= '<span class="breadcrumb-separator">&raquo;</span>';
						$breadcrumbs .= '<a href="' . esc_url( get_term_link( $ancestor ) ) . '">' . esc_html( $ancestor->name ) . '</a>';
					}
				}
				// Current taxonomy term
				$breadcrumbs .= '<span class="breadcrumb-separator"> &raquo; </span>';
				$breadcrumbs .= esc_html( $taxonomy->name );
			}
			$breadcrumbs .= '</div>';
			echo $breadcrumbs;
		}
		// Define the breadcrumb shortcode
		add_shortcode('webland_vanilla_shortcode_breadcrumbs', 'webland_vanilla_breadcrums');
		
	}

}
