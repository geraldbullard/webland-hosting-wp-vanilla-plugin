<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.weblandhosting.com/
 * @since      1.0.0
 * @package    WebLand_Vanilla
 * @subpackage WebLand_Vanilla/includes
 * @author     Gerald Bullard Jr <gbullard@weblandhosting.com>
 */
class WebLand_Vanilla_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		global $wpdb;
		$table_name = $wpdb->prefix . 'webland_vanilla';
		if (self::is_table_exists($table_name)) {
			// Later we will check if destroy data was selected
			// For now we just leave the table alone
			// self::delete_table($table_name);
		}
	}
	
	/**
	 * Check if vanilla table exists
	 */
	private function is_table_exists($table_name) {
		global $wpdb;
		return ( $wpdb->get_var( "SHOW TABLES LIKE '{$table_name}'" ) == $table_name );
	}

	/**
	 * Delete vanilla table
	 */
	private function delete_table($table_name) {
		global $wpdb;
		$wpdb->query("DROP TABLE IF EXISTS {$table_name}");
	}

}
