<?php

/**
 * @link              https://www.weblandhosting.com/
 * @since             1.0.0
 * @package           WebLand_Vanilla
 *
 * @wordpress-plugin
 * Plugin Name:       WebLand Vanilla Plugin
 * Plugin URI:        https://www.weblandhosting.com/vanilla/
 * Description:       WebLand Vanilla Plugin Framework
 * Version:           1.0.0
 * Author:            WebLand Hosting
 * Author URI:        https://www.weblandhosting.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       webland-vanilla
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'WEBLAND_VANILLA_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-webland-vanilla-activator.php
 */
function activate_webland_vanilla() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-webland-vanilla-activator.php';
	WebLand_Vanilla_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-webland-vanilla-deactivator.php
 */
function deactivate_webland_vanilla() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-webland-vanilla-deactivator.php';
	WebLand_Vanilla_Deactivator::deactivate();
}
register_activation_hook( __FILE__, 'activate_webland_vanilla' );
register_deactivation_hook( __FILE__, 'deactivate_webland_vanilla' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-webland-vanilla.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_webland_vanilla() {
	$plugin = new Webland_Vanilla();
	$plugin->run();
}
run_webland_vanilla();

// Later, let's see how much we can move to class files ////////////////////////////////////////////////////////////////
function webland_vanilla_settings_init() {
    add_settings_section(
        'webland-vanilla-settings-section', // id of the section
        NULL, // title to be displayed
        '', // callback function to be called when opening section
        'webland-vanilla-settings' // page on which to display the section, this should be the same as the slug used in add_submenu_page()
    );
    // register settings
    register_setting(
        'webland-vanilla-settings', // option group
        'webland_vanilla_apiuser_name'
    );
    add_settings_field(
        'webland-vanilla-apiuser_name', // id of the settings field
        'Registered To', // title
        'webland_vanilla_apiuser_name', // callback function
        'webland-vanilla-settings', // page on which settings display
        'webland-vanilla-settings-section' // section on which to show settings
    );
    register_setting(
        'webland-vanilla-settings', // option group
        'webland_vanilla_username'
    );
    add_settings_field(
        'webland-vanilla-username', // id of the settings field
        'WebLand Username', // title
        'webland_vanilla_username', // callback function
        'webland-vanilla-settings', // page on which settings display
        'webland-vanilla-settings-section' // section on which to show settings
    );
    register_setting(
        'webland-vanilla-settings', // option group
        'webland_vanilla_password'
    );
    add_settings_field(
        'webland-vanilla-password', // id of the settings field
        'WebLand Password', // title
        'webland_vanilla_password', // callback function
        'webland-vanilla-settings', // page on which settings display
        'webland-vanilla-settings-section' // section on which to show settings
    );
}
add_action('admin_init', 'webland_vanilla_settings_init');

// Plugin Settings page
function webland_vanilla_settings_page() {
    add_submenu_page(
        'options-general.php', // top level menu page
        'WebLand Vanilla Settings', // title of the settings page
        'WebLand Vanilla', // title of the submenu
        'manage_options', // capability of the user to see this page
        'webland-vanilla-settings', // slug of the settings page
        'webland_vanilla_settings_html' // callback function to be called when rendering the page
    );
}
add_action('admin_menu', 'webland_vanilla_settings_page');

// Some Individual Settings, until we can move to class files
function webland_vanilla_apiuser_name() {
    $webland_vanilla_apiuser_name = esc_attr(get_option('webland_vanilla_apiuser_name', ''));
    echo $webland_vanilla_apiuser_name;
}
function webland_vanilla_username() {
    $webland_vanilla_username = esc_attr(get_option('webland_vanilla_username', ''));
    ?>
    <input type="text" name="webland_vanilla_username" value="<?php echo $webland_vanilla_username; ?>">
    <?php
}
function webland_vanilla_password() {
    $webland_vanilla_password = esc_attr(get_option('webland_vanilla_password', ''));
    ?>
    <input type="password" name="webland_vanilla_password" value="<?php echo $webland_vanilla_password; ?>">
    <?php
}

// Settings link in plugin listing
function webland_vanilla_settings_link( $links ) {
    // Build and escape the Settings URL
    $url = esc_url( add_query_arg(
        'page',
        'webland-vanilla-settings',
        get_admin_url() . 'options-general.php'
    ) );
    // Create the link.
    $settings_link = "<a href='$url'>" . __( 'Settings' ) . '</a>';
    // Adds the link to the end of the array.
    array_push(
        $links,
        $settings_link
    );
    // Now set the Premium or Upgrade link
    // $webland_vanilla = new Webland_Vanilla();
    // if ($webland_vanilla->get_vanilla_verified() !== true) { // Upgrade
    //     $upgrade = esc_url( add_query_arg(
    //         '',
    //         '',
    //         'https://www.weblandhosting.com/'
    //     ) );
    //     $upgrade_link = '<a href=' . $upgrade . ' target="_blank" class="wl-red"><strong>Go Premium</strong></a>';
    //     // Adds the link to the end of the array.
    //     array_push(
    //         $links,
    //         $upgrade_link
    //     );
    // } else { // Premium
    //     $premium = esc_url( add_query_arg(
    //         '',
    //         '',
    //         'https://www.weblandhosting.com/'
    //     ) );
    //     $premium_link = '<a href=' . $premium . ' target="_blank" class="wl-green"><strong>WebLand Account</strong></a>';
    //     // Adds the link to the end of the array.
    //     array_push(
    //         $links,
    //         $premium_link
    //     );
    // }
    return $links;
}
add_filter( 'plugin_action_links_webland-vanilla/webland-vanilla.php', 'webland_vanilla_settings_link' );
